import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestM {

	private M m;
	private MockServer ms;

	public TestM() {
	}

	@Before
	public void setup() {
		ms = new MockServer();
		m = new M(ms);
	}

	@After
	public void tearDown() {
		m = null;
	}

	@Test
	public void testZeroMethod() {
		int result = m.zero();
		assertEquals(0, result);
	}

	@Test
	public void testFailIfZeroMethod_fails() {
		try {
			m.failIfZero(0);
			fail("Method should throw exception!");
		} catch (Exception e) {
			// Test Passes
		}
	}

	@Test
	public void testFailIfZeroMethod_passes() {
		m.failIfZero(1);
	}

	@Test
	public void testGetData() {
		String data = m.get_data();
		assertNotNull(data);
	}

}
